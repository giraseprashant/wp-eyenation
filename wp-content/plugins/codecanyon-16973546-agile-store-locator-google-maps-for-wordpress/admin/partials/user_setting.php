<!-- Container -->
<div class="asl-p-cont">
<div class="dump-message asl-dumper"></div>
<form class="form-horizontal" id="frm-usersetting">
	<div class="row" id="message_complete"></div>	
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title"><?php echo __('Agile Store Locator Pro Settings','asl_admin') ?></h3></div>
			  	<div class="panel-body">
			  		<div class="alert alert-info" role="alert">
				      <?php echo __('Support Forum for New features or Troubleshooting.','asl_admin') ?> <a target="_blank" href="https://wordpress.org/support/plugin/agile-store-locator">Create a Ticket</a> 
				    </div>
		           	<div class="form-group s-option no-0">
			            <label class="col-sm-3 control-label" for="txt_Cluster"><?php echo __('Cluster','asl_admin') ?></label>
			          	 <select  id="asl-cluster" name="data[cluster]" class="form-control">
			            	<option value="0"><?php echo __('OFF','asl_admin') ?></option>	
			            	<option value="1"><?php echo __('ON','asl_admin') ?></option>
				        </select>
		           	</div>
		           	<div class="form-group s-option no-1">
			            <label class="col-sm-3 control-label" for="map_type"><?php echo __('Default Map','asl_admin') ?></label>
			          	 <select   id="asl-map_type" name="data[map_type]" class="form-control">
			          	 	<option value="hybrid"><?php echo __('HYBRID','asl_admin') ?></option>
				            	<option value="roadmap"><?php echo __('ROADMAP','asl_admin') ?></option>
				            	<option value="satellite"><?php echo __('SATELLITE','asl_admin') ?></option>
				            	<option value="terrain"><?php echo __('TERRAIN','asl_admin') ?></option>
				        </select>
		           	</div>
		           	<div class="form-group s-option no-2">
			            <label class="col-sm-3 control-label" for="txt_time_format"><?php echo __('Time Format','asl_admin') ?></label>
			          	 <select  id="asl-time_format" name="data[time_format]" class="form-control">
			            	<option value="0"><?php echo __('12 Hours','asl_admin') ?></option>	
			            	<option value="1"><?php echo __('24 Hours','asl_admin') ?></option>
				        </select>
		           	</div>
		           	<div class="form-group s-option no-3">
			            <label class="col-sm-3 control-label" for="display_list"><?php echo __('Display List','asl_admin') ?></label>
			          	<select id="asl-display_list" name="data[display_list]" class="form-control">
			          	 	<option value="1"><?php echo __('Yes','asl_admin') ?></option>
				            <option value="0"><?php echo __('No','asl_admin') ?></option>
				        </select>
		           	</div>
		           	<div class="form-group s-option no-4 full">
			            <label class="col-sm-3 control-label" for="distance_control"><?php echo __('Distance Control','asl_admin') ?></label>
			          	 <select  id="asl-distance_control" name="data[distance_control]" class="form-control">
				         		<option value="0"><?php echo __('Slider','asl_admin') ?></option>	
				            	<option value="1"><?php echo __('Dropdown','asl_admin') ?></option>
				        </select>
				        <p class="help-p col-sm-offset-3 col-sm-9 red"><?php echo __('Choose Either Slider or Dropdown for Radius Miles/KM','asl_admin') ?><br>
				        <a target="_blank" class="red" href="https://agilestorelocator.com/wiki/set-radius-value-distance-range-slider/"><?php echo __('To Set Maximum value for Search Range Slider','asl_admin') ?></a></p>
		           	</div>
		           	<div class="row form-group no-of-shop">
			            <label class="col-sm-3 control-label"><?php echo __('Search Dropdown Options','asl_admin') ?></label>
						<div class="col-sm-8">
							<input type="text" class="form-control" name="data[dropdown_range]" id="asl-dropdown_range" placeholder="Example: 10,20,30">
						</div>
						<p class="help-p col-sm-offset-3 col-sm-9 red"><?php echo __('Enter the Search dropdown Options number, Comma Separated. Add default value with * symbol.','asl_admin') ?>
						Default Value Example: 10*,20,30 . Here 10 is default value</p>
		           	</div>
		           	<div class="form-group s-option no-4 full">
			            <label class="col-sm-3 control-label" for="prompt_location"><?php echo __('Prompt','asl_admin') ?></label>
			          	 <select  id="asl-prompt_location" name="data[prompt_location]" class="form-control">
				         		<option value="0"><?php echo __('NONE','asl_admin') ?></option>	
				         		<option value="1"><?php echo __('GEOLOCATION DIALOG','asl_admin') ?></option>	
				            	<option value="2"><?php echo __('TYPE YOUR LOCATION Dialog','asl_admin') ?></option>
				            	<option value="3"><?php echo __('GEOLOCATION WITHOUT DIALOG','asl_admin') ?></option>
				        </select>
				        <p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('GEOLOCATION ONLY WORKS WITH HTTPS CONNECTION','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group s-option no-4 full">
			            <label class="col-sm-3 control-label" for="search_destin"><?php echo __('Search Result','asl_admin') ?></label>
			          	 <select  id="asl-search_destin" name="data[search_destin]" class="form-control">
				         		<option value="0"><?php echo __('Default','asl_admin') ?></option>
				         		<option value="1"><?php echo __('Show My Nearest Location From Search','asl_admin') ?></option>	
				        </select>
				        <p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('In search address point to my nearest markers','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group s-option no-4 full">
			            <label class="col-sm-3 control-label" for="sort_by"><?php echo __('Sort List','asl_admin') ?></label>
			          	 <select  id="asl-sort_by" name="data[sort_by]" class="form-control">
				         		<option value=""><?php echo __('Default (Distance)','asl_admin') ?></option>
				         		<option value="title"><?php echo __('Store ID','asl_admin') ?></option>	
				         		<option value="title"><?php echo __('Title','asl_admin') ?></option>
				         		<option value="ordr"><?php echo __('Order Field','asl_admin') ?></option>	
				         		<option value="city"><?php echo __('City','asl_admin') ?></option>	
				         		<option value="state"><?php echo __('State','asl_admin') ?></option>	
				         		<option value="logo_id"><?php echo __('Logo ID','asl_admin') ?></option>	
				        </select>
				        <p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('Sort your listing based on fields, default is Distance','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group s-option no-4 full">
			            <label class="col-sm-3 control-label" for="stores_limit"><?php echo __('Stores Limit (Show Limit Stores Only)','asl_admin') ?></label>
			          	 <input  type="number" class="form-control validate[integer]" name="data[stores_limit]" id="asl-stores_limit">
				        <p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('Default is NULL, Option will display given Store count only, for only mobile use attribute mobile_stores_limit="10" in shortcode','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group s-option no-5">
			            <label class="col-sm-3 control-label" for="distance_unit"><?php echo __('Distance Unit','asl_admin') ?></label>
			          	 <select  id="asl-distance_unit" name="data[distance_unit]" class="form-control">
				            	<option value="KM"><?php echo __('KM','asl_admin') ?></option>	
				            	<option value="Miles"><?php echo __('Miles','asl_admin') ?></option>
				        </select>
		           	</div>
		           	<div class="form-group s-option no-6">
			            <label class="col-sm-3 control-label" for="asl-zoom"><?php echo __('Default Zoom','asl_admin') ?></label>
			          	 <select  id="asl-zoom" name="data[zoom]" class="form-control">
				            	<?php for($index = 2;$index <= 20;$index++):?>
				            		<option value="<?php echo $index ?>"><?php echo $index ?></option>
				            	<?php endfor; ?>
				        </select>
		           	</div>
					
					<div class="form-group s-option no-6">
			            <label class="col-sm-3 control-label" for="geo_button"><?php echo __('Field Button Type','asl_admin') ?></label>
			          	 <select  id="asl-geo_button" name="data[geo_button]" class="form-control">
				         	<option value="1"><?php echo __('Geo-Location','asl_admin') ?></option>
				            <option value="0"><?php echo __('Search Location','asl_admin') ?></option>
				        </select>
		           	</div>

		           	<div class="form-group s-option no-6">
			            <label class="col-sm-3 control-label" for="week_hours"><?php echo __('Display Hours','asl_admin') ?></label>
			          	 <select  id="asl-week_hours" name="data[week_hours]" class="form-control">
				            <option value="0"><?php echo __('Today','asl_admin') ?></option>
				         	<option value="1"><?php echo __('7 Days','asl_admin') ?></option>
				        </select>
		           	</div>

		           	<div class="form-group s-option no-7 full">
			            <label class="col-sm-3 control-label" for="asl-zoom_li"><?php echo __('Clicked Zoom','asl_admin') ?></label>
			          	 <select  id="asl-zoom_li" name="data[zoom_li]" class="form-control">
				            	<?php for($index = 2;$index <= 20;$index++):?>
				            		<option value="<?php echo $index ?>"><?php echo $index ?></option>
				            	<?php endfor; ?>
				        </select>
				        <p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('Zoom Value when List Item is Clicked, use zoom_li="10" in ShortCode','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group s-option no-7 full">
			            <label class="col-sm-3 control-label" for="asl-search_zoom"><?php echo __('Search Zoom','asl_admin') ?></label>
			          	 <select  id="asl-search_zoom" name="data[search_zoom]" class="form-control">
				            	<?php for($index = 2;$index <= 20;$index++):?>
				            		<option value="<?php echo $index ?>"><?php echo $index ?></option>
				            	<?php endfor; ?>
				        </select>
				        <p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('Zoom value when Search is performed','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group s-option no-7 full">
			            <label class="col-sm-3 control-label" for="search_type"><?php echo __('Search Type','asl_admin') ?></label>
			          	 <select  name="data[search_type]" id="asl-search_type" class="form-control">
				            	<option value="0"><?php echo __('Search By Address (Google)','asl_admin') ?></option>
				            	<option value="1"><?php echo __('Search By Store Name (Database)','asl_admin') ?></option>
				            	<option value="3"><?php echo __('Search By Stores Cities, States (Database)','asl_admin') ?></option>
				        </select>
				        <p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('Search by Either Address or Store Name, use search_type="1" in ShortCode','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group s-option no-7 full">
			            <label class="col-sm-3 control-label" for="load_all"><?php echo __('Marker Load','asl_admin') ?></label>
			          	 <select  name="data[load_all]" id="asl-load_all" class="form-control">
				            	<option value="1"><?php echo __('Load All','asl_admin') ?></option>
			          	 		<option value="2"><?php echo __('Load Markers with Search Area Button','asl_admin') ?></option>
				            	<option value="0"><?php echo __('Load on Bound','asl_admin') ?></option>
				        </select>
				        <p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('Use Load on Bound in case of 1000 plus markers','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group s-option no-7 full">
			            <label class="col-sm-3 control-label" for="single_cat_select"><?php echo __('Category Select','asl_admin') ?></label>
			          	 <select  name="data[single_cat_select]" id="asl-single_cat_select" class="form-control">
				            	<option value="0"><?php echo __('Multiple Category Selection','asl_admin') ?></option>
				            	<option value="1"><?php echo __('Single Category Selection','asl_admin') ?></option>
				        </select>
		           	</div>
		           	<div class="form-group s-option no-7 full">
			            <label class="col-sm-3 control-label"><?php echo __('Search Field','asl_admin') ?></label>
			          	 <select  name="data[google_search_type]" id="asl-google_search_type" class="form-control">
				            	<option value=""><?php echo __('All','asl_admin') ?></option>
				            	<option value="cities"><?php echo __('Cities (Cities)','asl_admin') ?></option>
				            	<option value="regions"><?php echo __('Regions (Locality, City, State)','asl_admin') ?></option>
				            	<option value="geocode"><?php echo __('Geocode','asl_admin') ?></option>
				            	<option value="address"><?php echo __('Address','asl_admin') ?></option>
				        </select>
		           	</div>       
		           	<div class="form-group s-option no-7 full">
			            <label class="col-sm-3 control-label"><?php echo __('Full Height','asl_admin') ?></label>
			          	 <select  name="data[full_height]" id="asl-full_height" class="form-control">
				            	<option value=""><?php echo __('None','asl_admin') ?></option>
				            	<option value="full-height"><?php echo __('Full Height (Not Fixed)','asl_admin') ?></option>
				            	<option value="full-height asl-fixed"><?php echo __('Full Height (Fixed)','asl_admin') ?></option>
				        </select>
		           	</div>

		           	<div class="form-group s-option no-7 full">
			            <label class="col-sm-3 control-label" for="Template"><?php echo __('Map Region','asl_admin') ?></label>
			          	 <select  id="asl-map_region" name="data[map_region]" class="form-control">
				         	<option value=""><?php echo __('None','asl_admin') ?></option>	
			            	<?php foreach($countries as $country): ?>
			            		<option value="<?php echo $country->code ?>"><?php echo $country->country ?></option>
			            	<?php endforeach ?>
				        </select>
		           	</div>

                    <div class="row form-group no-of-shop">
			            <label class="col-sm-3 control-label"><?php echo __('Restrict Search','asl_admin') ?></label>
						<div class="col-sm-8">
							<input  type="text" class="form-control validate[minSize[2]]" maxlength="2" name="data[country_restrict]" id="asl-country_restrict" placeholder="Example: US">
						</div>
						<p class="help-p col-sm-offset-3 col-sm-9">(Enter 2 alphabet country <a href="https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2" target="_blank" rel="nofollow">Code</a>)</p>
		           	</div>

                    <div class="row form-group no-of-shop">
			            <label class="col-sm-3 control-label"><?php echo __('Map Language','asl_admin') ?></label>
						<div class="col-sm-8">
							<input type="text" class="form-control validate[minSize[2]]" maxlength="2" name="data[map_language]" id="asl-map_language" placeholder="Example: US">
						</div>
						<p class="help-p col-sm-offset-3 col-sm-9">(<?php echo __('Enter the Language Code.','asl_admin') ?> <a href="https://agilestorelocator.com/wiki/display-maps-different-language/" target="_blank" rel="nofollow">Get Code</a>)</p>
		           	</div>
                    <br clear="both">

		           	<div class="form-group c-option no-0 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-show_categories"><?php echo __('Show Categories','asl_admin') ?></label>
			          	<input type="checkbox" value="1" id="asl-show_categories" name="data[show_categories]">
		           	</div>
		           	<div class="form-group c-option no-1 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-time_switch"><?php echo __('Time Switch','asl_admin') ?></label>
			          	<input type="checkbox" value="1" id="asl-time_switch" name="data[time_switch]">
		           	</div>
		           	<!--
		           	<div class="form-group">
			            <label class="col-sm-3 control-label" for="asl-direction">Direction Service</label>
			          	<input type="checkbox" value="1" id="asl-direction" name="data[direction]">
		           	</div>
		           	-->
		           	<div class="form-group c-option no-2 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-additional_info"><?php echo __('Additional Info','asl_admin') ?></label>
			          	<input type="checkbox" value="1" id="asl-additional_info" name="data[additional_info]">
		           	</div>
		           	<div class="form-group c-option no-3 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-advance_filter"><?php echo __('Advance Filter','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-advance_filter" name="data[advance_filter]">
		           	</div>
		           	<div class="form-group c-option no-4 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-category_marker"><?php echo __('Category marker','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-category_marker" name="data[category_marker]">
			          	<p class="help-p red">(<?php echo __('Single Category Selection','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group c-option no-5 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-distance_slider"><?php echo __('Distance Control','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-distance_slider" name="data[distance_slider]">
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-full_width"><?php echo __('Full Width','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-full_width" name="data[full_width]">
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-analytics"><?php echo __('Analytics','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-analytics" name="data[analytics]">
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-scroll_wheel"><?php echo __('Mouse Scroll','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-scroll_wheel" name="data[scroll_wheel]">
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-sort_by_bound"><?php echo __('Sort By Bound','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-sort_by_bound" name="data[sort_by_bound]">
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-target_blank"><?php echo __('Open Link New Tab','asl_admin') ?></label>
			          	<input type="checkbox" value="1" id="asl-target_blank" name="data[target_blank]">
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-user_center"><?php echo __('Default Location Marker','asl_admin') ?></label>
			          	<input type="checkbox" value="1" id="asl-user_center" name="data[user_center]">
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-smooth_pan"><?php echo __('Smooth Marker Pan','asl_admin') ?></label>
			          	<input type="checkbox" value="1" id="asl-smooth_pan" name="data[asl-smooth_pan]">
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart hide">
			            <label class="col-sm-3 control-label" for="asl-filter_result"><?php echo __('Filter Location Result (Beta)','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-filter_result" name="data[filter_result]">
			          	<p class="help-p red">(<?php echo __('Shows only Matched Results of Search Like City, Zip, State.','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-remove_maps_script"><?php echo __('Remove Other Maps Scripts','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-remove_maps_script" name="data[remove_maps_script]">
			          	<p class="help-p red">(<?php echo __('Remove Multiple inclusion of Google Maps.','asl_admin') ?>)</p>
		           	</div>
		           	<div class="form-group c-option no-6 thirdpart">
			            <label class="col-sm-3 control-label" for="asl-radius_circle"><?php echo __('Radius Circle','asl_admin') ?></label>
			          	<input type="checkbox" value="1"  id="asl-radius_circle" name="data[radius_circle]">
			          	<p class="help-p red">(<?php echo __('Radius Circle will only appear with Dropdown Control.','asl_admin') ?>)</p>
		           	</div>
		           	<br clear="both">
		           	<br clear="both">
		           	<div class="form-group page_layout">
			            <label class="col-sm-3 control-label" ><?php echo __('Layout','asl_admin') ?></label>
						<div id="radio">
						    <input type="radio" id="asl-layout-0" value="0" name="data[layout]"><label for="asl-layout-0"><img src="<?php echo ASL_URL_PATH ?>/admin/images/layout_1.png" /></label>
						    <input type="radio" id="asl-layout-1" value="1" name="data[layout]"><label for="asl-layout-1"><img src="<?php echo ASL_URL_PATH ?>/admin/images/layout_2.png" /></label>
					  	</div>
		           	</div>
		           	<br clear="both">
		           	<div class="form-group lng-lat">
			            <label class="col-sm-3 control-label"><?php echo __('Default Lat/Lng','asl_admin') ?></label>
						<div class="col-sm-4">
							<input  type="text" class="form-control validate[required]" name="data[default_lat]" id="asl-default_lat" placeholder="Lat">
						</div>
						<div class="col-sm-4">
						<input  type="text" class="form-control validate[required]" name="data[default_lng]"  id="asl-default_lng" placeholder="Lng">
						</div>
						<p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('Default coordinates for map to load','asl_admin') ?>)</p>
		           	</div>
		           	<br clear="both">
		           	<div class="row form-group no-of-shop">
			            <label class="col-sm-3 control-label"><?php echo __('Header Title','asl_admin') ?></label>
						<div class="col-sm-8">
							<input  type="text" class="form-control validate[required]" name="data[head_title]" id="asl-head_title" placeholder="Head title">
						</div>
		           	</div>
                    <br clear="both">

                    <div class="row form-group no-of-shop">
			            <label class="col-sm-3 control-label"><?php echo __('Category Title','asl_admin') ?></label>
						<div class="col-sm-8">
							<input  type="text" class="form-control validate[required]" name="data[category_title]" id="asl-category_title" placeholder="Category title">
						</div>
		           	</div>
                    <br clear="both">

                    <div class="row form-group no-of-shop">
			            <label class="col-sm-3 control-label"><?php echo __('No Item Text','asl_admin') ?></label>
						<div class="col-sm-8">
							<input  type="text" class="form-control validate[required]" name="data[no_item_text]" id="asl-no_item_text" placeholder="No Item Text">
						</div>
		           	</div>
                    <br clear="both">

                    <div class="row form-group no-of-shop">
			            <label class="col-sm-3 control-label"><?php echo __('Google API KEY','asl_admin') ?></label>
						<div class="col-sm-8">
							<input  type="text" class="form-control" name="data[api_key]" id="asl-api_key" placeholder="API KEY">
						</div>
						<p class="help-p col-sm-offset-3 col-sm-9 red">(<?php echo __('Generate API Key using Google Console','asl_admin') ?>)</p>
		           	</div>
                    <br clear="both">
                    
                    <div class="form-group map_layout">
			            <label class="col-sm-3 control-label" ><?php echo __('Map Layout','asl_admin') ?></label>
						<div id="radio">
						    <input type="radio" id="asl-map_layout-0" value="0" name="data[map_layout]"><label for="asl-map_layout-0"><img src="<?php echo ASL_URL_PATH ?>admin/images/map/25-blue-water/25-blue-water.png" /></label>
						    <input type="radio" id="asl-map_layout-1" value="1" name="data[map_layout]"><label for="asl-map_layout-1"><img src="<?php echo ASL_URL_PATH ?>admin/images/map/Flat Map/53-flat-map.png" /></label>
						    <input type="radio" id="asl-map_layout-2" value="2" name="data[map_layout]"><label for="asl-map_layout-2"><img src="<?php echo ASL_URL_PATH ?>admin/images/map/Icy Blue/7-icy-blue.png" /></label>
						    <input type="radio" id="asl-map_layout-3" value="3" name="data[map_layout]"><label for="asl-map_layout-3"><img src="<?php echo ASL_URL_PATH ?>admin/images/map/Pale Dawn/1-pale-dawn.png" /></label>
						    <input type="radio" id="asl-map_layout-4" value="4" name="data[map_layout]"><label for="asl-map_layout-4"><img src="<?php echo ASL_URL_PATH ?>admin/images/map/cladme/6618-cladme.png" /></label>
						    <input type="radio" id="asl-map_layout-5" value="5" name="data[map_layout]"><label for="asl-map_layout-5"><img src="<?php echo ASL_URL_PATH ?>admin/images/map/light monochrome/29-light-monochrome.png" /></label>
						    <input type="radio" id="asl-map_layout-6" value="6" name="data[map_layout]"><label for="asl-map_layout-6"><img src="<?php echo ASL_URL_PATH ?>admin/images/map/mostly grayscale/4183-mostly-grayscale.png" /></label>
						    <input type="radio" id="asl-map_layout-7" value="7" name="data[map_layout]"><label for="asl-map_layout-7"><img src="<?php echo ASL_URL_PATH ?>admin/images/map/turquoise water/8-turquoise-water.png" /></label>
						    <input type="radio" id="asl-map_layout-8" value="8" name="data[map_layout]"><label for="asl-map_layout-8"><img src="<?php echo ASL_URL_PATH ?>admin/images/map/unsaturated browns/70-unsaturated-browns.png" /></label>
					  	</div>
		           	</div>
                    
                    <div class="form-group" style="width:100%">
			            <label class="col-sm-3 control-label" for="Template"><?php echo __('Template','asl_admin') ?></label>
			          	 <select  id="asl-template" name="data[template]" class="form-control" style="width:200px">
				         		<option value="0" data-src="<?php echo ASL_URL_PATH ?>admin/images/template-0.jpg">Template 0</option>	
				         		<option value="1" data-src="<?php echo ASL_URL_PATH ?>admin/images/template-1.jpg">Template 1</option>	
				         		<option value="2" data-src="<?php echo ASL_URL_PATH ?>admin/images/template-1.jpg">Template 2</option>
				        </select>
		           	</div>
		           	<label class="layout-box" style="width: 50%; clear: both; margin-left: 27%; display: inline-block; margin-top: -10px;">
                        <img src="<?php echo ASL_URL_PATH ?>admin/images/template-0.jpg" style="max-width: 100%">
                        <img src="<?php echo ASL_URL_PATH ?>admin/images/template-1.jpg" style="max-width: 100%">
                        <img src="<?php echo ASL_URL_PATH ?>admin/images/template-2.jpg" style="max-width: 100%">
                    </label>
                    <br clear="both">
                    
                    
                    <div class="template-box box_layout_0 hide"> <div class="row form-group color_scheme">
				            <label class="col-sm-3 control-label"><?php echo __('Color Scheme','asl_admin') ?></label>
							<div id="radio" >
                                <span>
                                    <input type="radio" id="asl-color_scheme-0" value="0" name="data[color_scheme]">
                                    <label class="color-box color-0" for="asl-color_scheme-0"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme-1" value="1" name="data[color_scheme]">
                                    <label class="color-box color-1" for="asl-color_scheme-1"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme-2" value="2" name="data[color_scheme]">
                                    <label class="color-box color-2" for="asl-color_scheme-2"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme-3" value="3" name="data[color_scheme]">
                                    <label class="color-box color-3" for="asl-color_scheme-3"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme-4" value="4" name="data[color_scheme]">
                                    <label class="color-box color-4" for="asl-color_scheme-4"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme-5" value="5" name="data[color_scheme]">
                                    <label class="color-box color-5" for="asl-color_scheme-5"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme-6" value="6" name="data[color_scheme]">
                                    <label class="color-box color-6" for="asl-color_scheme-6"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme-7" value="7" name="data[color_scheme]">
                                    <label class="color-box color-7" for="asl-color_scheme-7"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme-8" value="8" name="data[color_scheme]">
                                    <label class="color-box color-8" for="asl-color_scheme-8"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme-9" value="9" name="data[color_scheme]">
                                    <label class="color-box color-9" for="asl-color_scheme-9"></label>
                                </span>
                            </div>
			           	</div>
			           	<div class="row form-group Font_color">
				            <label class="col-sm-3 control-label"><?php echo __('Font Colors','asl_admin') ?></label>
							<div id="radio">
                                <span>
                                    <input type="radio" id="asl-font_color_scheme-0" value="0" name="data[font_color_scheme]">
                                    <label class="font-color-box color-0" for="asl-font_color_scheme-0"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-font_color_scheme-1" value="1" name="data[font_color_scheme]">
                                    <label class="font-color-box color-1" for="asl-font_color_scheme-1"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-font_color_scheme-2" value="2" name="data[font_color_scheme]">
                                    <label class="font-color-box color-2" for="asl-font_color_scheme-2"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-font_color_scheme-3" value="3" name="data[font_color_scheme]">
                                    <label class="font-color-box color-3" for="asl-font_color_scheme-3"></label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-font_color_scheme-4" value="4" name="data[font_color_scheme]">
                                    <label class="font-color-box color-4" for="asl-font_color_scheme-4"></label>
                                </span>
                            </div>
			           	</div>
                        <div class="form-group infobox_layout">
				            <label class="col-sm-3 control-label" ><?php echo __('Infobox Layout','asl_admin') ?></label>
							<div id="radio">
							    <input type="radio" id="asl-infobox_layout-0" value="0" name="data[infobox_layout]"><label for="asl-infobox_layout-0"><img src="<?php echo ASL_URL_PATH ?>/admin/images/infobox_1.png" /></label>
							    <input type="radio" id="asl-infobox_layout-2" value="2" name="data[infobox_layout]"><label for="asl-infobox_layout-2"><img src="<?php echo ASL_URL_PATH ?>/admin/images/infobox_2.png" /></label>
							    <input type="radio" id="asl-infobox_layout-1" value="1" name="data[infobox_layout]"><label for="asl-infobox_layout-1"><img src="<?php echo ASL_URL_PATH ?>/admin/images/infobox_3.png" /></label>
						  	</div>
			           	</div>
                    </div>
                    <div class="template-box box_layout_1 hide">
			           	<div class="row form-group color_scheme layout_2">
				            <label class="col-sm-3 control-label"><?php echo __('Color Scheme','asl_admin') ?></label>
							<div id="radio" >
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-0" value="0" name="data[color_scheme_1]">
                                    <label class="color-box color-0" for="asl-color_scheme_1-0">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-1" value="1" name="data[color_scheme_1]">
                                    <label class="color-box color-1" for="asl-color_scheme_1-1">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-2" value="2" name="data[color_scheme_1]">
                                    <label class="color-box color-2" for="asl-color_scheme_1-2">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-3" value="3" name="data[color_scheme_1]">
                                    <label class="color-box color-3" for="asl-color_scheme_1-3">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-4" value="4" name="data[color_scheme_1]">
                                    <label class="color-box color-4" for="asl-color_scheme_1-4">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-5" value="5" name="data[color_scheme_1]">
                                    <label class="color-box color-5" for="asl-color_scheme_1-5">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-6" value="6" name="data[color_scheme_1]">
                                    <label class="color-box color-6" for="asl-color_scheme_1-6">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-7" value="7" name="data[color_scheme_1]">
                                    <label class="color-box color-7" for="asl-color_scheme_1-7">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-8" value="8" name="data[color_scheme_1]">
                                    <label class="color-box color-8" for="asl-color_scheme_1-8">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_1-9" value="9" name="data[color_scheme_1]">
                                    <label class="color-box color-9" for="asl-color_scheme_1-9">
                                        <span class="co_1"></span>
                                        <span class="co_2"></span>
                                    </label>
                                </span>
                            </div>
			           	</div>
                    </div>
                    <div class="template-box box_layout_2 hide">
			           	<div class="row form-group color_scheme layout_2">
				            <label class="col-sm-3 control-label"><?php echo __('Color Scheme','asl_admin') ?></label>
							<div id="radio" >
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-0" value="0" name="data[color_scheme_2]">
                                    <label class="color-box color-0" for="asl-color_scheme_2-0" style="background-color:#CC3333">
                                        <span class="co_1" style="background-color:#542733" ></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-1" value="1" name="data[color_scheme_2]">
                                    <label class="color-box color-1" for="asl-color_scheme_2-1" style="background-color:#008FED">
                                        <span class="co_1" style="background-color:#2580C3"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-2" value="2" name="data[color_scheme_2]">
                                    <label class="color-box color-2" for="asl-color_scheme_2-2" style="background-color:#93628F">
                                        <span class="co_1" style="background-color:#4A2849"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-3" value="3" name="data[color_scheme_2]">
                                    <label class="color-box color-3" for="asl-color_scheme_2-3" style="background-color:#FF9800">
                                        <span class="co_1" style="background-color:#FFC107"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-4" value="4" name="data[color_scheme_2]">
                                    <label class="color-box color-4" for="asl-color_scheme_2-4" style="background-color:#01524B">
                                        <span class="co_1" style="background-color:#75C9D3"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-5" value="5" name="data[color_scheme_2]">
                                    <label class="color-box color-5" for="asl-color_scheme_2-5" style="background-color:#ED468B">
                                        <span class="co_1" style="background-color:#FDCC29"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-6" value="6" name="data[color_scheme_2]">
                                    <label class="color-box color-6" for="asl-color_scheme_2-6" style="background-color:#D55121">
                                        <span class="co_1" style="background-color:#FB9C6C"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-7" value="7" name="data[color_scheme_2]">
                                    <label class="color-box color-7" for="asl-color_scheme_2-7" style="background-color:#D13D94">
                                        <span class="co_1" style="background-color:#AD0066"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-8" value="8" name="data[color_scheme_2]">
                                    <label class="color-box color-8" for="asl-color_scheme_2-8" style="background-color:#99BE3B">
                                        <span class="co_1" style="background-color:#01735A"></span>
                                    </label>
                                </span>
                                <span>
                                    <input type="radio" id="asl-color_scheme_2-9" value="9" name="data[color_scheme_2]">
                                    <label class="color-box color-9" for="asl-color_scheme_2-9" style="background-color:#3D5B99">
                                        <span class="co_1" style="background-color:#EFF1F6"></span>
                                    </label>
                                </span>
                            </div>
			           	</div>
                    </div>

		           	<div class="form-group ralign">
			            <button type="button" class="btn btn-primary mrg-r-10" data-loading-text="<?php echo __('Saving...','asl_admin') ?>" data-completed-text="Settings Updated" id="btn-asl-user_setting"><?php echo __('Save Settings','asl_admin') ?></button>
		           	</div>
				</div>
			</div>
		</div>
		<div class="col-md-5 ">
			<h3 class="title"><?php echo __('FREQUENTLY ASKED QUESTIONS','asl_admin') ?></h3>
			<ul class="asl-faq">
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/store-locator-doesnot-appear/">Why doesn't Store Locator appear at all with the shortcode?</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/custom-color-store-locator/">My required color scheme is not available in the Color Pallet.</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/remove-hours-store-list/">How to remove hours from the Store List?</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/blog/ssl-must-geolocation-api-chrome-browser/">Why Geo-Location feature not working?</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/can-import-stores-using-excel-sheet/">How can I import using Excel Sheet?</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/change-words-text-store-locator/">How can I change the words of Store Locator?</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/load-google-maps-app-mobile-direction/">How to load direction in Google Maps App instead of Store Locator?</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/change-address-format/">How can I change Address format?</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/blog/customize-google-marker-infowindow-sidebar-store-locator/">How to show additional information in listing or Info-Window?</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/accordion-layout-stores-not-assigned-properly/">In accordion Layout, the Stores are not assigned properly to Cities.</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/show-limited-stores-sort-by-distance/">How to Show only 10 Stores sort by distance?</a></li>
				<li><a target="_blank" href="https://agilestorelocator.com/wiki/"><< DOCUMENTATION >> </a></li>
			</ul>
		    <div class="alert alert-info" role="alert">
		      If you have any problem with the plugin or suggestion,<br> please email us at <a  href="mailto:support@agilelogix.com">support@agilelogix.com</a><br> 
		      We will respond as soon as possible to resolve your problem, please include ("Store Locator" in the Subject) to avoid spam list.
		    </div>
		    <div class="alert alert-info" role="alert">
		      If you like our Plugin, please rate us 5 stars. <a target="_blank" href="https://codecanyon.net/item/agile-store-locator-google-maps-for-wordpress/reviews/16973546"><span style="margin-top: 2px"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span></a> 
		    </div>
		</div>
	</div>
</form>
</div>

<!-- SCRIPTS -->
<script type="text/javascript">
	var ASL_Instance = {
		url: '<?php echo ASL_URL_PATH ?>'
	},
	asl_configs =  <?php echo json_encode($all_configs); ?>;
	asl_engine.pages.user_setting(asl_configs);
</script>